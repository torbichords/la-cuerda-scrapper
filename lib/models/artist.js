const _ = require('underscore');

const mongoose = require('mongoose');
mongoose.connect(`mongodb://${process.env.NODE_ENV == 'docker' ? 'mongo' : 'localhost'}/la-cuerda-scrapper`);
mongoose.Promise = require('q').Promise;

const Schema = mongoose.Schema;

const VersionSchema = new Schema({
	type: String,
	number: Number,
	url: String,
	text: String,
	html: String,
	modificationDate: { type: Date, default: Date.now }
});

const SongSchema = new Schema({
	name: String,
	identifier: String,
	url: String,
	versions: [VersionSchema],
	modificationDate: { type: Date, default: Date.now }
});

const ArtistSchema = new Schema({
	name: String,
	identifier: String,
	url: String,
	songs: [SongSchema],
	modificationDate: { type: Date, default: Date.now }
});

ArtistSchema.statics.saveArtist = (artist) => {
	return mongoose.model('Artist').findOne({ identifier: artist.identifier }).exec().then(existingArtist => {
		if(existingArtist) {
			return existingArtist;
		} else {
			return this.create(artist);
		}
	});
};

ArtistSchema.statics.updateArtistSongs = (artist, songs) => {
	let artistChanged = false;

	_.each(songs, song => {
		const existingSong = _.find(artist.songs, existing => {
			return existing.identifier === song.identifier;
		});

		if(existingSong) {
			_.each(song.versions, version => {
				const existingVersion = _.find(existingSong.versions, existing => {
					return existing.number == version.number;
				})

				if(!existingVersion) {
					existingSong.versions.push(version);
					existingSong.modificationDate = Date.now();
					artistChanged = true;
				}
			})
		} else {
			artist.songs.push(song);
			artistChanged = true;
		}
	});

	if(artistChanged) {
		artist.modificationDate = Date.now();
		return artist.save();
	} else {
		return artist;
	}
};

ArtistSchema.statics.updateArtistSongVersions = (artist, song, versions) => {
	song.versions = _.map(song.versions, existingVersion => {
		const version = _.find(versions, v => {
			return v.number == existingVersion.number;
		});

		if(version) {
			existingVersion.html = version.html;
			existingVersion.text = version.text;
		}

		return existingVersion;
	});

	return artist.save();
};

const ArtistModel = mongoose.model('Artist', ArtistSchema);

module.exports = ArtistModel;