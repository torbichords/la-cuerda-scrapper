module.exports = function (Downloader) {
	const _ = require('underscore');
	const logger = require('winston');

	const Database = require('./database');
	const Artist = require('./models/artist');
	const Q = require('./ext-q');

	function scrap(options) {
		logger.log('info', 'scrapping...');

		return Q.serial(scrapIndex, options.ARTISTS_INDEX).then(results => {
			return _.union.apply(this, results);
		});
	}

	function scrapIndex(ref) {
		logger.log('info', 'scrapping reference ' + ref + '...');

		return Downloader.downloadIndex(ref).then(index => {
			// Guardar los artistas en la base de datos
			return Q.all(_.map(index.artists, Database.saveArtist));
		}).then(artists => {
			// Scrap de cada artista
			return Q.serial(scrapArtist, artists);
		});
	}

	function scrapArtist(artist) {
		logger.log('info', 'scrapping artist ' + artist.name + '...');

		return Downloader.downloadArtist(artist).then(songs => {
			// Agregar las canciones nuevas y agregar las nuevas versiones a las canciones existentes
			return Artist.updateArtistSongs(artist, songs);
		}).then(updatedArtist => {
			// Scrap de cada cancion
			return Q.serial(_.partial(scrapArtistSong, updatedArtist, _), updatedArtist.songs);
		});
	}

	function scrapArtistSong(artist, song) {
		logger.log('info', 'scrapping song ' + song.name + '...');

		const versionsToDownload = _.filter(song.versions, version => {
			return !version.html;
		});

		if(versionsToDownload.length > 0) {
			return Q.all(_.map(versionsToDownload, scrapVersion)).then(versions => {
				return Artist.updateArtistSongVersions(artist, song, versions);
			});
		} else {
			const deferred = Q.defer();
			deferred.resolve(artist);
			return deferred.promise;
		}
	}

	function scrapVersion(version) {
		logger.log('info', 'scrapping version ' + version.number + '...');

		return Downloader.downloadVersion(version).then(versionContent => {
			return versionContent;
		});
	}

	return {
		scrap: scrap
	};
};

