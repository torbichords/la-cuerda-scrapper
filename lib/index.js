// Hack para poder usar Jquery sin el objeto window
require('jsdom').env('', (err, window) => {
	if (err) {
		console.error(err);
		throw err;
	}

	const _ = require('underscore');
	const logger = require('winston');

	const $ = require('jquery')(window);
	const Parser = require('./parser')($);
	const Downloader = require('./downloader')(Parser);
	const Scrapper = require('./scrapper')(Downloader);

	const args = require('./args');

	// LLamado inicial
	start();

	function start() {
		Scrapper.scrap({
			ARTISTS_INDEX: args.ARTISTS_INDEX
		}).then(artists => {
			process.exit(0);
		}).catch(errorHandler);
	}

	function sortArtists(artists) {
		let artistsData = artists;

		if(args.DOWNLOAD_POPULARITY) {
			artistsData = _.chain(artistsData)
				.map(artist => {
					const songs = _.sortBy(artist.songs, song => {
						return -song.versions.length;
					});

					return {
						artist: artist.artist,
						songs: songs
					}
				})
				.sortBy(artist => {
					return -artist.songs.length;
				})
				.value();
		}

		return artistsData;
	}

	function getDownloadData(artists) {
		return _.flatten(_.map(artists, artist => {
			return _.map(artist.songs, (song, songIndex) => {
				return _.map(song.versions, (version, versionIndex) => {
					return {
						version: version,
						versionIndex: versionIndex,
						song: song,
						songIndex: songIndex,
						artist: artist
					};
				});
			});
		}));
	}

	/**
	 * Error handler por defecto
	 * @param err Error
	 */
	function errorHandler(err) {
		logger.error(err);
		process.exit(1);
	}
});



