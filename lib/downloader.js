module.exports = function (Parser) {
	const _ = require('underscore');
	const http = require('http');
	const iconv = require('iconv-lite');
	const logger = require('winston');

	const Q = require('./ext-q');

	const args = require('./args');

	function downloadArtistsIndexFirstPage(reference) {
		return downloadPath("/tabs/" + reference + "/index0.html").then(html => {
			const remainingPages = Parser.findRemainingPages(html);
			const artists = Parser.parseArtistsIndexPage(html);

			return {
				reference: reference,
				remainingPages: remainingPages,
				artists: artists
			};
		});
	}

	function downloadArtistsIndexRemainingPages(data) {
		const promises = _.map(data.remainingPages, p => {
			return downloadPath("/tabs/" + data.reference + "/" + p).then(Parser.parseArtistsIndexPage);
		});

		return Q.all(promises).then(artists => {
			const allArtists = _.chain(artists).flatten().concat(data.artists).uniq().value();

			return {
				reference: data.reference,
				artists: allArtists
			};
		});
	}

	function downloadIndex(ref) {
		return downloadArtistsIndexFirstPage(ref).then(data => {
			if (data.remainingPages.length > 0) {
				return downloadArtistsIndexRemainingPages(data);
			} else {
				return {
					reference: data.reference,
					artists: data.artists
				};
			}
		});
	}

	function downloadArtist(artist) {
		return downloadPath(artist.url).then(_.partial(Parser.parseSongsIndexPage, _, artist));
	}

	function downloadVersions(versions) {
		return Q.serial(downloadVersion, versions);
	}

	function downloadVersion(version) {
		logger.log('info', 'Downloading from ' + version.url);

		return downloadPath(version.url).then(Parser.parseVersion).then(result => {
			return _.extend(version, result);
		});
	}

	function downloadPath(path) {
		return downloadPageTry(path, 3);
	}

	function downloadPageTry(path, remaining) {
		return downloadPage(path).fail(e => {
			if(remaining > 0) {
				return downloadPageTry(path, remaining--);
			} else {
				throw new Error(e);
			}
		});
	}

	/**
	 * Devuelve el contenido de una pagina web
	 * @param path url a descargar
	 * @returns {Promise<String>}
	 */
	function downloadPage(path) {
		const deferred = Q.defer();

		const options = {
			host: "acordes.lacuerda.net",
			path: path,
			headers: {},
			timeout: 5 * 60 * 1000
		};

		const req = http.request(options, r => {
			let rawData = Buffer.from([]);

			r.on('data', chunk => {
				rawData = Buffer.concat([rawData, chunk]);
			});

			r.on('end', () => {
				deferred.resolve(iconv.decode(rawData, 'iso-8859-1'));
			});
		});

		req.on('error', (e) => {
			deferred.reject(e);
		});

		req.end();

		return deferred.promise;
	}

	return {
		downloadIndex: downloadIndex,
		downloadArtist: downloadArtist,
		downloadVersion: downloadVersion,
		downloadVersions: downloadVersions
	};
};



