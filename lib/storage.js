const fs = require('fs');
const Q = require('q');
const _ = require("underscore");

function saveTablature(tabInfo) {
	const directories = ['tabs', tabInfo.reference, tabInfo.artist];

	return createDirectories(directories).then(_.partial(writeFile, _, tabInfo));
}

function writeFile(directory, tabInfo) {
	const deferred = Q.defer();

	const filename = tabInfo.song + '_' + tabInfo.version + '_' + tabInfo.type + '.txt';

	fs.writeFile(directory + '/' + filename, tabInfo.tab, 'utf8', err => {
		if(err) {
			deferred.reject(err.message);
		} else {
			deferred.resolve();
		}
	});

	return deferred.promise;
}

function createDirectories(directories) {
	const deferred = Q.defer();

	if(directories.length === 0) {
		deferred.reject('Invalid number of directories to create');
	} else {
		fs.access(directories[0], 'rw', err => {
			if(err) {
				if(err.code === 'ENOENT') {
					fs.mkdir(directories[0], 0o777, err => {
						if(err) {
							deferred.reject(err.message);
						} else {
							if(directories.length === 1) {
								deferred.resolve(directories[0]);
							} else {
								directories[1] = directories[0] + '/' + directories[1];
								createDirectories(directories.slice(1)).then(d => {
									deferred.resolve(d);
								});
							}
						}
					});
				} else {
					deferred.reject('No read-write permission in directory ' + directories[0]);
				}
			} else {
				if(directories.length === 1) {
					deferred.resolve(directories[0]);
				} else {
					directories[1] = directories[0] + '/' + directories[1];
					createDirectories(directories.slice(1)).then(d => {
						deferred.resolve(d);
					});
				}
			}
		});
	}

	return deferred.promise;
}

module.exports = {
	save: saveTablature
};