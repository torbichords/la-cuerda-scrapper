const Q = require('q');
const _ = require('underscore');

Q.serial = function(func, params) {
	const deferred = Q.defer();
	const results = [];

	if(params.length > 0) {
		const firstPromise = func(params[0]).then(result => {
			results.push(result);
			deferred.notify(result);
		});
		const promise = _.chain(params.slice(1))
			.map(param => {
				return _.partial(func, param);
			})
			.reduce((acc, partial) => {
				return acc.then(partial).then(result => {
					results.push(result);
					deferred.notify(result);
				});
			}, firstPromise)
			.value();

		promise.then(() => {
			deferred.resolve(results);
		});

		promise.catch(e => {
			deferred.reject(e);
		});
	} else {
		deferred.resolve(results);
	}

	return deferred.promise;
};

module.exports = Q;