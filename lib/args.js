const _ = require('underscore');
const argv = require('yargs')['argv'];

// Argumentos
let ARTISTS_INDEX = ['_num', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'nn', 'o', 'p',
	'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];
if (argv['letters'] && argv['letters'].length > 0) {
	ARTISTS_INDEX = _.chain(argv['letters'].split('')).map(traduceLetter).uniq().value();
}

const DOWNLOAD_POPULARITY = !!argv.popularity || !!argv.p;
const DOWNLOAD_TABS = DOWNLOAD_POPULARITY || !!argv.download || !!argv.d;

function traduceLetter(letter) {
	let result = letter.toLowerCase();

	switch (result) {
		case '#':
			result = '_num';
			break;
		case 'ñ':
			result = 'nn';
			break;
	}

	return result;
}

module.exports = {
	ARTISTS_INDEX: ARTISTS_INDEX,
	DOWNLOAD_TABS: DOWNLOAD_TABS,
	DOWNLOAD_POPULARITY: DOWNLOAD_POPULARITY
};