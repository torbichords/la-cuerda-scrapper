const _ = require('underscore');
const Q = require('./ext-q');

const Artist = require('./models/artist');

function saveArtist(scrappedArtist) {
	return Artist.findOne({ identifier: scrappedArtist.identifier }).exec().then(artist => {
		if(artist) {
			return artist;
		} else {
			return Artist.create(scrappedArtist);
		}
	});
}

module.exports = {
	saveArtist: saveArtist
};