function Parser(jQuery) {
	const _ = require('underscore');
	const $ = jQuery;

	function parseArtistsIndexPage(html) {
		const aElements = $(html).find('#i_main li a');

		return _.map(aElements, (a) => {
			$(a).find('*').remove();

			const identifier = $(a).attr('href').replace(/\/(.*)\//, '$1');
			const name = $(a).text().trim();

			return {
				identifier: identifier,
				name: name,
				url: '/' + identifier + '/',
				songs: []
			};
		});
	}

	function findRemainingPages(html) {
		const remainingPagesElements = $(html).find('.m .multipag:nth-of-type(1) tr td a');

		return _.map(remainingPagesElements, p => {
			return $(p).attr('href');
		});
	}

	function parseSongsIndexPage(html, artist) {
		const songElements = $(html).find('#b_main li');

		return _.map(songElements, _.partial(parseSongElement, _, artist));
	}

	function parseSongElement(se, artist) {
		const name = $(se).find('a').text();
		const identifier = $(se).find('a').attr('href');
		const songUrl = artist.url + identifier + '/';

		const id = $(se).attr('id');
		const versionTypes = id.slice(id.indexOf('-', 2) + 1, id.indexOf('-', 6)).split('');
		const versionNumbers = id.substr(id.indexOf('-', 6) + 1).split('');

		const versions = [];
		for (let i = 0; i < versionTypes.length; i++) {
			let versionUrl = artist.url + identifier;
			if(versionNumbers[i] > 1) {
				versionUrl += '-' + versionNumbers[i];
			}
			versionUrl += '.shtml';

			const version = {
				number: versionNumbers[i],
				type: versionTypes[i],
				url: versionUrl
			};

			versions.push(version);
		}

		return {
			name: name,
			identifier: identifier,
			versions: versions,
			url: songUrl
		};
	}

	function parseVersion(html) {
		const tabElement = $(html).find('div#t_body pre');

		const version = {
			text: $(tabElement).text(),
			html: $(tabElement).html()
		};

		return version;
	}

	return {
		parseArtistsIndexPage: parseArtistsIndexPage,
		findRemainingPages: findRemainingPages,
		parseSongsIndexPage: parseSongsIndexPage,
		parseVersion: parseVersion
	};
}

module.exports = Parser;