#!/usr/bin/env bash

cd /root/app/la-cuerda-scrapper

npm install

export NODE_ENV='docker'

node ./index.js --d
